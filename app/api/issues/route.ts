

import { NextRequest, NextResponse } from "next/server";
import { z } from "zod"; // data validation using ZOD - refer to ZOD documentation
import prisma from "@/prisma/client";

const createIssueSchema = z.object({
    title: z.string().min(1).max(255),
    description: z.string().min(1)
})

// @path        /issues/new
// @route       public
// @method      POST
export async function POST(request: NextRequest){
    const body  = await request.json()
    const issueValidation = createIssueSchema.safeParse(body)

    // Checking the validation
    if (!issueValidation.success) {
        return NextResponse.json(issueValidation.error.errors, { status: 400 })
    }

    const newIssue = await prisma.issue.create({
        data: { title: body.title, description: body.description  }
    });

    return NextResponse.json(newIssue, { status: 201 })
}

// @path        /issues/new
// @route       public
// @method      GET
export async function GET(request: NextRequest) {
    return NextResponse.json({ status: 200 })
    
}

// @path        /issues/
// @route       public
// @method      DELETE
export async function DELETE(request: NextRequest) {
    return NextResponse.json({status: 401})
}

// @path        /issues/
// @route       public
// @method      PATCH
export async function PATCH(request: NextRequest) {
    return NextResponse.json
}

