'use client'
import Link from 'next/link'
import React from 'react'
import { VscDebugAll } from "react-icons/vsc";
import { usePathname } from 'next/navigation';
import classnames from "classnames"

const NavBar = () => {
    // Active path to pages
    const currentPath = usePathname()

    const links = [
        { label: 'Dashboard', href: '/' },
        { label: 'Issues', href: '/issues' }
    ]

  return (
    <nav className='flex space-x-10 border-b-2 mb-5 px-5 h-14 items-center bg-slate-50 '>
        <Link href="/"><VscDebugAll className='text-2xl' />
        </Link>
        <ul className='flex space-x-4'>
            {links.map(link => 
            <Link 
                key={link.href}
                className={classnames({
                    'text-zinc-900': link.href === currentPath,
                    'text-zinc-500': link.href !== currentPath,
                    'hover:text-zinc-800 transition-colors': true
                })

                 }
                href={link.href}>{link.label}
            </Link> )}
        </ul>
    </nav>
  )
}

export default NavBar