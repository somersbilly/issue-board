An issue board built to highlight common issues faced during the census operations. 

## Public Use
This app is license under the GPL and is free to customize and use. Follow the common install processes of cloning and starting a Next JS app.

## Deploy on AWS
A development version will be deployed to AWS for access.


